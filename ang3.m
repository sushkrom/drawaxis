function ang3(center, A, B, radius, name, planeNormal, arrowheadscale)
% Draw great arc on sphere
% of radius r from A to B

if nargin < 4
    radius = 1;
end
if nargin < 5
    name = '';
end
if nargin < 6
    planeNormal = [];
end
if nargin < 7
    arrowheadscale = 1;
end

holdStatus = ishold();
hold on

center = center';
A = A';
B = B';
% A = A - center; % vectors are assumed to start in center
% B = B - center;

r=radius;
A=r*A/norm(A);
B=r*B/norm(B);

% angle subtended by great arc
% from point A to point B
if size(planeNormal) == 0
    planeNormal = cross(A,B);
end
theta=acos(dot(A,B)/r^2);
thetaDir = 1;
if dot(planeNormal, cross(A,B)) < 0
  theta = -pi - theta;
  thetaDir = -1;
end


% normal, binormal, and tangent
% vectors at point A
N=A/r; BN=cross(A,B);
T=cross(BN,N); T=T/norm(T);
BT=cross(BN,B);

n=20; t=linspace(0,theta,n)';
N=repmat(N,n,1); T=repmat(T,n,1);
t=repmat(t,1,3);
garc=r*(N.*cos(t)+T.*sin(t));
garc_center = garc + repmat(center,n,1);
plot3(garc_center(:,1),garc_center(:,2),garc_center(:,3),...
   'k','linewidth', 1)

% draw arrow
arrowhead = B;
% arrowshoulder1 = B - 0.1*B - 7*BT;
% arrowshoulder2 = B + 0.1*B - 7*BT;
% set shoulders width
arrowshoulder1 = B - 0.1*r*B/norm(B)*arrowheadscale;
arrowshoulder2 = B + 0.1*r*B/norm(B)*arrowheadscale;
% set shloulders depth
arrowshoulder1 = arrowshoulder1 - 0.2*r*BT/norm(BT)*arrowheadscale*thetaDir;
arrowshoulder2 = arrowshoulder2 - 0.2*r*BT/norm(BT)*arrowheadscale*thetaDir;
% define arrow polyline
arrowline = [arrowshoulder2; arrowhead; arrowshoulder1];
arrowline_center = arrowline + repmat(center,3,1);
plot3(arrowline_center(:,1), arrowline_center(:,2), arrowline_center(:,3), 'color', 'black', 'linewidth', 1);

% put name
name_pos = (B/norm(B) + A/norm(A))/sqrt(2)*r*1.2 + center;
text(name_pos(1), name_pos(2), name_pos(3), name);

if ~holdStatus
    hold off
end

end