function drawaxisline( ax , name, whichax, linewidth)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2
    name = '';
end
if nargin < 3
    whichax = 'x';
end
if nargin < 4
    linewidth = 2;
end

if whichax == 'x'
    color = 'r';
elseif whichax == 'y'
    color = 'g';
elseif whichax == 'z'
    color = 'b';
else
    error('bad whichax; use x y or z');
end

plot3(ax(1,:), ax(2,:), ax(3,:), color, 'linewidth', linewidth)

text(ax(1,2), ax(2,2), ax(3,2), [whichax '_{' name '}'], 'color', color)


end

