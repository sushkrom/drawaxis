A0 = eye(4);
A1 = [rotx(pi/6)*roty(pi/3)*rotx(pi/6) [1; 1; 1]; 0 0 0 1];


figure;
hold on;

arcradius = 0.3;

[O_0, x0, y0, z0] = drawaxis(A0, '0', 'xyz');
[O_1, x1, y1, z1] = drawaxis(A1, '1', 'xyz');
drawaxisline([O_0 O_0+y1], '1', 'y');
ang3(O_0, y0, y1, arcradius, '\alpha_1');
