function [O_0, x, y, z] = drawaxis(A, name, selectAxis, axislength, linewidth)
%DRAWAXIS draw axis (position is defined by homogenious transformation A)
%   A : homogenious transformation
%   name : axis name (shown as index) : example : name = 1 => axis is shown
%   as x1 x2 x3
%   selectAxis : draw x, y, (or/and) z axis. default : selectAxis = 'xyz'. if
%   selectAxis == 'x', draw only x axis

if nargin < 2
    name = '';
end
if nargin < 3
    selectAxis = 'xyz';
end
if nargin < 4
    axislength = 1;
end
if nargin < 5
    linewidth = 2;
end

xax = [0 1; 0 0; 0 0]*axislength;
yax = [0 0; 0 1; 0 0]*axislength;
zax = [0 0; 0 0; 0 1]*axislength;

% homogenious
xax = [xax; 1 1];
yax = [yax; 1 1];
zax = [zax; 1 1];

xaxNew = A * xax;
yaxNew = A * yax;
zaxNew = A * zax;

O_0 = A*[0;0;0;1];
x = xaxNew(:,2)-O_0;
y = yaxNew(:,2)-O_0;
z = zaxNew(:,2)-O_0;
O_0 = O_0(1:3);
x = x(1:3);
y = y(1:3);
z = z(1:3);

if strfind(selectAxis, 'x')
    drawaxisline(xaxNew, name, 'x', linewidth);
end
if strfind(selectAxis, 'y')
    drawaxisline(yaxNew, name, 'y', linewidth);
end
if strfind(selectAxis, 'z')
    drawaxisline(zaxNew, name, 'z', linewidth);
end
if isempty([strfind(selectAxis, 'x') strfind(selectAxis, 'y') strfind(selectAxis, 'z')])
    error('selectAxis is expected to contain x, y, (and/or) z');
end


end

